﻿using UnityEngine;
using System.Collections;

public class SnapToPixel : MonoBehaviour
{

    void LateUpdate()
    {
        float s = 0.1f;


        Vector3 p = transform.position;
        p.x -= 0.05f;
        p.y -= 0.05f;
        p /= s;
        p = new Vector3(Mathf.Round(p.x), Mathf.Round(p.y), Mathf.Round(p.z));
        p *= s;
        p.x += 0.05f;
        p.y += 0.05f;

        //p.y -= 0.025f;
        //p.x -= 0.025f;

        transform.position = p;
    }
}
