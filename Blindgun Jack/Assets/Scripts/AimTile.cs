﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class AimTile : MonoBehaviour
{
    public Transform[] points;

   public  BezierPath path = new BezierPath();

    void Start()
    {
        init();
    }

    bool _init = false;
    public void init()
    {
        if (_init)
            return;

        _init = true;
        List<Vector3> tmp = new List<Vector3>();
        foreach (Transform t in points)
            tmp.Add(t.position);

        path.SetControlPoints(tmp);

        path.Interpolate(path.GetDrawingPoints0(), 0.1f);
    }
    void OnDrawGizmos()
    {
        _init = false;
        init();

        List<Vector3> p = path.GetDrawingPoints0();

        for(int i=0;i<p.Count-1;i++)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(p[i], p[i + 1]);
        }

    }

   
}
