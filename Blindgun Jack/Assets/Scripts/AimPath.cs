﻿using UnityEngine;
using System.Collections.Generic;

public class AimPath : MonoBehaviour
{
    GameObject[] prefabs;
    public Vector3 nextPoint;
    Vector3 nextDirection = Vector3.forward;

    List<GameObject> objects = new List<GameObject>();
    List<BezierPath> paths = new List<BezierPath>();
    List<float> directions = new List<float>();

    public GameObject gunsightPrefab;

    public GameObject gunsight;

    float startTime = 0;
    void Start()
    {
        prefabs = Resources.LoadAll<GameObject>("aim/");

        nextPoint = transform.position;
    }

    public void clearPath()
    {
        foreach (GameObject obj in objects)
            Destroy(obj);

        objects.Clear();
        paths.Clear();
        directions.Clear();

        nextPoint = transform.position;
        nextDirection = Vector3.forward;
    }

    public void pushRandomTiles(int length)
    {
        for (int i = 0; i < length; i++)
            addTile(Random.Range(0,prefabs.Length), Random.Range(-1f,1f));
    }

    public void addTile(int i, float direction)
    {
        direction = Mathf.Sign(direction);

        GameObject obj = Instantiate(prefabs[i]);
        obj.name = i.ToString();
        obj.transform.rotation = Quaternion.identity;
        
        AimTile tile = obj.GetComponent<AimTile>();

        int a = 0;
        int b = tile.points.Length - 1;
        if (direction < 0f)
        {
            b = 0;
            a = tile.points.Length - 1;
        }

        Quaternion angle = Quaternion.LookRotation(Vector3.forward, nextDirection);
        Quaternion angle2 = Quaternion.LookRotation(Vector3.forward, tile.points[a].forward * direction);
        Quaternion delta = Quaternion.Inverse(angle2) * angle;
        obj.transform.rotation = delta;

        Vector3 p = obj.transform.position - obj.transform.TransformPoint(tile.points[a].localPosition);
        obj.transform.position = nextPoint + p;

        nextPoint = tile.points[b].position;
        nextDirection = tile.points[b].forward*direction;

        objects.Add(obj);
        paths.Add(tile.path);
        directions.Add(direction);

        tile.init();
    }

    public void removeLast()
    {
        if (paths.Count == 0)
            return;

        AimTile tile = objects[objects.Count - 1].GetComponent<AimTile>();
        int a = 0;
        int b = tile.points.Length - 1;

        if (directions[directions.Count-1]<0f)
        {
            a = b;
            b = 0;
        }

        nextPoint = tile.points[a].position;
        nextDirection = tile.points[a].forward * directions[directions.Count-1];


        Destroy(objects[objects.Count-1]);

        objects.RemoveAt(objects.Count-1);
        paths.RemoveAt(paths.Count - 1);
        directions.RemoveAt(directions.Count - 1);
    }

    public int getLast()
    {
        if (paths.Count == 0)
            return -1;

        return int.Parse(objects[objects.Count - 1].name);
    }
    public float getLastDirection()
    {
        if (paths.Count == 0)
            return 1f;

        return directions[directions.Count-1];
    }

    public delegate void OnMovementComplete();
    OnMovementComplete onMovementComplete;
    public void startMovement(OnMovementComplete _onMovementComplete)
    {
        if(paths.Count==0)
        {
            _onMovementComplete();
            return;
        }

        onMovementComplete = _onMovementComplete;
        moveI = 0;
        curveI = 0;
        offset = 0;
        if(directions[0]<0f)
            curveI=(paths[0].GetControlPoints().Count / 3) -1;

        if(gunsight==null)
            gunsight = Instantiate(gunsightPrefab);
        startTime = Time.time;
    }

    public bool moving
    {
        get
        {
            return moveI > 0f;
        }
    }

    int moveI = -1;
    int curveI = 0;
    int curveCount;
    float offset = 0f;
    void Update()
    {
        move();
    }

    void move()
    {
        if (moveI < 0)
            return;

        curveCount = paths[moveI].GetControlPoints().Count/3;

        float t = (Time.time - startTime)/(1f/45f);
        t += offset;
        float ii = t;
        if (directions[moveI] < 0f)
            ii = 1f - t;
        gunsight.transform.position = paths[moveI].CalculateBezierPoint(curveI, Mathf.Clamp01(ii))-Vector3.forward;
        if (t >= 1f)
        {
            if (directions[moveI] > 0f)
                curveI++;
            else
                curveI--;

            if( (directions[moveI] > 0f && curveI >= curveCount) || (directions[moveI] < 0f && curveI < 0) )
            {
                moveI++;
                curveI = 0;
                if (moveI < paths.Count && directions[moveI] < 0f)
                    curveI = paths[moveI].GetControlPoints().Count / 3 - 1;
            }
            offset = t - 1f;
            startTime = Time.time;
            if (moveI >= paths.Count)
            {
                moveI = -1;
                Destroy(gunsight);
                if (onMovementComplete != null)
                    onMovementComplete();
            }
        }
    }
}
