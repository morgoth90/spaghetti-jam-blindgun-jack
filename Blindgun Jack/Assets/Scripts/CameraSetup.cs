﻿using UnityEngine;
using System.Collections;

public class CameraSetup : MonoBehaviour
{
    public new MeshRenderer renderer;

	void Awake()
    {
        RenderTexture rt = new RenderTexture(800, 600, 24);
        rt.filterMode = FilterMode.Point;

        renderer.material.mainTexture = rt;

        GetComponent<Camera>().targetTexture = rt;
    }
}
