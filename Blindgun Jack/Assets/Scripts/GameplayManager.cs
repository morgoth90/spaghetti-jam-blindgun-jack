﻿using UnityEngine;
using System.Collections;

public class GameplayManager : MonoBehaviour
{
    public AudioClip music;

    public AimPath aimPath;

    public CanvasGroup topCanvasGroup, bottomCanvasGroup;

    public UnityEngine.UI.Text enemyText;
    public UnityEngine.UI.Text[] answersTexts;
    public UnityEngine.UI.Image[] answersIcons;

    TextAsset[] texts;

    public Transform pathArea;
    public Transform[] enemyAreas;

    public GameObject[] characterAnimations;

    void Start()
    {
        AudioSource source = Camera.main.GetComponent<AudioSource>();
        source.clip = music;
        source.loop = true;
        source.Play();

        texts = Resources.LoadAll<TextAsset>("texts/");

        startRandomPhase();

        topCanvasGroup.alpha = 0;
        bottomCanvasGroup.alpha = 0;

        characterAnimations[0].SetActive(true);
        characterAnimations[1].SetActive(false);
        characterAnimations[2].SetActive(false);
    }

    void startRandomPhase()
    {
        startPhase(texts[Random.Range(0, texts.Length)]);
    }

    void startPhase(TextAsset text)
    {
        topVisible = true;
        bottomVisible = true;
        int i = 0;
        foreach (string line in text.text.Split(new char[] { '\n' }))
        {
            if (i == 0)
                enemyText.text = line;
            else
            {
                answersTexts[i - 1].text = line;
            }
            i++;
        }
    }

    bool topVisible = false;
    bool bottomVisible = false;
    void keyPressed(int key)
    {
        if (key == 2)
        {
            int last = aimPath.getLast();
            if (last >= 0)
            {
                float lastDirection = aimPath.getLastDirection();

                aimPath.removeLast();

                aimPath.addTile(last, -lastDirection);
            }
            nextPhase();
            return;
        }

        if (key == 3)
        {
            shoot();
            return;
        }

        aimPath.pushRandomTiles(1);
        nextPhase();
    }

    void nextPhase()
    {
        Vector2 pos = (Vector2)pathArea.position;
        Vector2 size = (Vector2)pathArea.localScale;
        Rect aimRect = new Rect(pos - size / 2f, size);
        if (aimRect.Contains((Vector2)aimPath.nextPoint) == false)
        {
            shoot();
            return;
        }

        topVisible = false;
        bottomVisible = false;
        delay(delegate ()
        {
            startRandomPhase();
        }, 0.5f);

    }

    void shoot()
    {
        characterAnimations[0].SetActive(false);
        characterAnimations[1].SetActive(true);
        characterAnimations[2].SetActive(false);

        topVisible = false;
        bottomVisible = false;
        aimPath.startMovement(delegate ()
        {
            aimPath.clearPath();

            nextPhase();

            characterAnimations[0].SetActive(true);
            characterAnimations[1].SetActive(false);
            characterAnimations[2].SetActive(false);
        });
    }

    void fire()
    {
        characterAnimations[0].SetActive(false);
        characterAnimations[1].SetActive(false);
        characterAnimations[2].SetActive(false);

        characterAnimations[2].SetActive(true);

        foreach (Transform enemyArea in enemyAreas)
        {
            Vector2 pos = (Vector2)enemyArea.position;
            Vector2 size = (Vector2)enemyArea.localScale;
            Rect enemyRect = new Rect(pos - size / 2f, size);
            if (enemyRect.Contains((Vector2)aimPath.gunsight.transform.position))
            {
                Instantiate(Resources.Load("hit"), aimPath.gunsight.transform.position, Quaternion.identity);
                return;
            }
        }
        Instantiate(Resources.Load("miss"), aimPath.gunsight.transform.position, Quaternion.identity);
    }

    void gameOver()
    {
        topVisible = false;
        bottomVisible = false;
    }

    void Update()
    {
        if (bottomVisible)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                keyPressed(1);
            if (Input.GetKeyDown(KeyCode.Alpha2))
                keyPressed(2);
            if (Input.GetKeyDown(KeyCode.Alpha3))
                keyPressed(3);
        }
        else if(aimPath.gunsight!=null)
        {
            if (Input.GetMouseButtonDown(0))
                fire();
        }

        float fadeSpeed = 2f;

        if (topVisible)
            topCanvasGroup.alpha = Mathf.MoveTowards(topCanvasGroup.alpha, 1f, Time.deltaTime*fadeSpeed*2f);
        else
            topCanvasGroup.alpha = Mathf.MoveTowards(topCanvasGroup.alpha, 0f, Time.deltaTime * fadeSpeed);

        if (bottomVisible)
            bottomCanvasGroup.alpha = Mathf.MoveTowards(topCanvasGroup.alpha, 1f, Time.deltaTime * fadeSpeed * 2f);
        else
            bottomCanvasGroup.alpha = Mathf.MoveTowards(topCanvasGroup.alpha, 0f, Time.deltaTime * fadeSpeed);
    }

    void delay(DelayDelegate onDelay, float delay)
    {
        StartCoroutine(_delay(onDelay, delay));
    }
    delegate void DelayDelegate();
    IEnumerator _delay(DelayDelegate onDelay, float delay)
    {
        yield return new WaitForSeconds(delay);

        if(onDelay!=null)
            onDelay();
    }
}
