﻿using UnityEngine;
using System.Collections;

public class DelayDestroy : MonoBehaviour
{
    public float delay = 0;

	void Start ()
    {
        Destroy(gameObject, delay);
	}
	
}
